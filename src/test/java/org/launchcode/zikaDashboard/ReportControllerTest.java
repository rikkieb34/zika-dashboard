package org.launchcode.zikaDashboard;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ReportRepository reportRepository;

    @Before
    public void setUp() {
        reportRepository.deleteAll();
    }

    @After
    public void tearDown() {
        reportRepository.deleteAll();
    }

    @Test
    public void testRouteWorks() throws Exception {
        this.mockMvc.perform(get("/report/")).andExpect(status().isOk());
    }

    @Test
    public void testOneReportReturnsOneElement() throws Exception {
        Report report = reportRepository.save(new Report("2016-04-02","Brazil-Rondonia","state","zika_reported","BR0011",
                "NA","NA",618,"cases"));
        this.mockMvc.perform(get("/report/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features", hasSize(1)));
    }

    @Test
    public void testAllAttributesAreOnTheGeoJSON() throws Exception {
        Report report = reportRepository.save(new Report("2016-04-02","Brazil-Rondonia","state","zika_reported","BR0011",
                "NA","NA",618,"cases"));
        this.mockMvc.perform(get("/report/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features[0].properties.reportDate", equalTo("2016-04-02")))
                .andExpect(jsonPath("$.features[0].properties.location", equalTo("Brazil-Rondonia")))
                .andExpect(jsonPath("$.features[0].properties.locationType", equalTo("state")))
                .andExpect(jsonPath("$.features[0].properties.dataField", equalTo("zika_reported")))
                .andExpect(jsonPath("$.features[0].properties.dataFieldCode", equalTo("BR0011")))
                .andExpect(jsonPath("$.features[0].properties.timePeriod", equalTo("NA")))
                .andExpect(jsonPath("$.features[0].properties.timePeriodType", equalTo("NA")))
                .andExpect(jsonPath("$.features[0].properties.zikaCount", equalTo(618)))
                .andExpect(jsonPath("$.features[0].properties.unit", equalTo("cases")));
    }
}
