package org.launchcode.zikaDashboard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class IndexControllerTest {
    @Autowired
    MockMvc mockMVC;

    @Test
    public void testIndexLoads() throws Exception {
        this.mockMVC.perform(get("/"))
                .andExpect(status().isOk());
    }
}
