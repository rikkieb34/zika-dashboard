package org.launchcode.zikaDashboard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ReportRepository reportRepository;

//    @Before
//    public void setUp() {
//        reportRepository.deleteAll();
//    }
//
//    @After
//    public void tearDown() {
//        reportRepository.deleteAll();
//    }

    @Test
    public void testGetReportByDate() throws Exception {
        this.mockMvc.perform(get("/api/reports?date=2016-03-05"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features", hasSize(2612)));
    }

    @Test
    public void testGetDates() throws Exception {
        this.mockMvc.perform(get("/api/reports/dates"))
                .andExpect(status().isOk());
    }
}
