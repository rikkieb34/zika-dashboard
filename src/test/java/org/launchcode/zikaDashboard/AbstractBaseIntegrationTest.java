package org.launchcode.zikaDashboard;

import org.junit.After;
import org.launchcode.zikaDashboard.data.ReportDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractBaseIntegrationTest {

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @After
    public void clearItemDocumentRepository() {
        reportDocumentRepository.deleteAll();
    }
}