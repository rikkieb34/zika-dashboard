package org.launchcode.zikaDashboard;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.junit.BeforeClass;
import org.junit.Test;
import org.launchcode.zikaDashboard.features.WktHelper;
import org.launchcode.zikaDashboard.models.Report;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class ReportTest {
    static Report report;

    @BeforeClass
    public static void setUp() throws Exception {
        List<String> record = getReport();
        report = new Report(record.get(0), record.get(1), record.get(2), record.get(3),
                record.get(4), record.get(5), record.get(6), Integer.parseInt(record.get(7)), record.get(8));
    }

    public static List<String> getReport() throws Exception {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("Brazil_Zika.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.replace("\"", "").split(",");
                records.add(Arrays.asList(values));
            }
        }
        return records.get(0);
    }

    public static Geometry wktToGeometry(String wktPoint) {
        WKTReader fromText = new WKTReader();
        Geometry geom = null;
        try {
            geom = fromText.read(wktPoint);
        } catch (ParseException e) {
            throw new RuntimeException("Not a WKT string:" + wktPoint);
        }
        return geom;
    }

    @Test
    public void testDate() {
        assertTrue(report.getReportDate().equals("2016-04-02"));
    }

    @Test
    public void testLocation() {
        assertTrue(report.getLocation().equals("Brazil-Rondonia"));
    }

    @Test
    public void testLocationType() {
        assertTrue(report.getLocationType().equals("state"));
    }

    @Test
    public void testDataField() {
        assertTrue(report.getDataField().equals("zika_reported"));
    }

    @Test
    public void testDataFieldCode() {
        assertTrue(report.getDataFieldCode().equals("BR0011"));
    }

    @Test
    public void testTimePeriod() {
        assertTrue(report.getTimePeriod().equals("NA"));
    }

    @Test
    public void testTimePeriodType() {
        assertTrue(report.getTimePeriodType().equals("NA"));
    }

    @Test
    public void testCount() {
        assertTrue(report.getZikaCount().equals(618));
    }

    @Test
    public void testUnit() {
        assertTrue(report.getUnit().equals("cases"));
    }
}
