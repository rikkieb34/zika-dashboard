package org.launchcode.zikaDashboard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRepoistoryTest {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testGetAllReports() {
        List<Report> reports = reportRepository.findAll();
        assertEquals( 220459,reports.size());
    }

    @Test
    public void testGetSingleReport() {
        Long number = Long.valueOf(4);
        Report report = reportRepository.getOne(number);
        assertEquals("Brazil-Amazonas", report.getLocation());
    }
}
