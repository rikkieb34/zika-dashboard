package org.launchcode.zikaDashboard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportDocumentControllerTest extends AbstractBaseRestIntegrationTest{
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testFuzzySearch() throws Exception {
        Report report = new Report("2012-02-03", "Brazil-Acre", "State", "zika-not-found",
                "NOPROB", "NA", "NA",3, "cases");
        String json = json(report);
        mockMvc.perform(post("/api/reports/")
                .content(json)
                .contentType(contentType));
        mockMvc.perform(get("/api/reports/search?q={term}", "Acre"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.features", hasSize(1)))
                .andExpect(jsonPath("$.features[0].properties.location").value(report.getLocation()));
    }
}
