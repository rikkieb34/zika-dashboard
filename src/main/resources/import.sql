BEGIN;

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;
CREATE EXTENSION IF NOT EXISTS unaccent;

COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, zika_count, unit) FROM '/tmp/all_reports-cleaned.csv' DELIMITER ',' CSV HEADER;
COPY location(name, city, location_geometry) FROM '/tmp/locations-cleaned.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, zika_count, unit, location_geometry) FROM '/tmp/El_Salvador_Zika.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, zika_count, unit, location_geometry) FROM '/tmp/Haiti_Zika.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, zika_count, unit, location_geometry) FROM '/tmp/Mexico_Zika.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, zika_count, unit, location_geometry) FROM '/tmp/MINSA_ZIKA.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, zika_count, unit, location_geometry) FROM '/tmp/Panama_Zika.csv' DELIMITER ',' CSV HEADER;

--UPDATE location SET city_normal = unaccent(city);
--UPDATE report set location_normal = unaccent(location);

COMMIT;
