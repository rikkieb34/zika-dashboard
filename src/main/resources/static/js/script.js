let locationsViewed = [];
let map;
let geoserverLayers = [];
let reportLayer;

$(document).ready(function () {

    $.ajax({
        url: "/api/reports/dates",
        type: 'GET',
        success: function(res) {
            res.forEach(function(date) {
                if(date != "2018-01-20") {
                    $('#dateSelect').append(`
                    <option value = ${date}>${date}</option>`)
                }
            });
        }
    })

    let zikaData = [{}];
    let changeCount = 0;

    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM({
        }),
        visible: true
    });

    const attribution =  new ol.control.Attribution({
        collapsible: true
    });

    //this is using an API key from http://www.thunderforest.com/docs/apikeys/
    //create a vars.js file and return the key from the getKey()
    var openCycleMapLayer = new ol.layer.Tile({
        source: new ol.source.OSM({
          attributions: [
            'All maps © <a href="https://www.opencyclemap.org/">OpenCycleMap</a>',
          ],
//          url: 'https://{a-c}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png' +
//              '?apikey=' + getKey()
//          url: 'https://tile.thunderforest.com/spinal-map/{z}/{x}/{y}.png' +
//                        '?apikey=' + getKey()
        })
      });

      var openSeaMapLayer = new ol.layer.Tile({
              source: new ol.source.OSM({
                attributions: [
                  'All maps © <a href="http://www.openseamap.org/">OpenSeaMap</a>',
                ],
                opaque: false,
                url: 'https://tiles.openseamap.org/seamark/{z}/{x}/{y}.png'
              })
            });

    map = new ol.Map({
        target: 'mapPlaceholder',
        layers: [
            osmLayer,
            openCycleMapLayer,
            openSeaMapLayer
        ],
        controls: ol.control.defaults({attribution: false}).extend([attribution]),
        view: new ol.View({
            center: ol.proj.fromLonLat([260.55, 23]),
            zoom: 4,
            minZoom: 2,
            maxZoom:10
        })
    });

    var zoomslider = new ol.control.ZoomSlider();
    map.addControl(zoomslider);

    const pointStyle = new ol.style.Style({
            image: new ol.style.Circle({
                radius: 5,
                fill: null,
                stroke: new ol.style.Stroke({color: 'blue', width: 1})
            })
    });
    let reportSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: '/api/reports/search/date?date=2018-01-20'
    });

    const styleFunction = function(feature) {
        const zikaCount = feature.get('zikaCount');
            let color = '';
            let radius = 1;
            let scale = 0;
            if(zikaCount <= 10) {
                color = 'rgba(0, 0, 255, .4)';
                radius = 5;
                scale = .5;
            } else if(zikaCount <= 50) {
                color = 'rgba(255, 140, 0, .4)';
                radius = 8;
                scale = .7;
            } else {
                color = 'rgba(255, 0, 0, .4)';
                radius = 11;
                scale = 1;
            }
            const strokeStyle = {color: color, width: 1}

            return new ol.style.Style({
//                            image: new ol.style.Icon({
//                                src: 'js/image/mosquito.png',
//                                color: color,
//                                scale: scale
//                                }),
                  stroke: new ol.style.Stroke({
                        color: color,
                        width: 1
                      }),
                      fill: new ol.style.Fill({
                        color: color,
                      })
          });
    }

    reportLayer = new ol.layer.Vector({
        source: reportSource,
        style: styleFunction
    });
    map.addLayer(reportLayer);

    reportLayer.addEventListener("change", function(event) {
        console.log('change', changeCount);
        if(changeCount %2 === 0 || changeCount === 0) {
            if(reportLayer.getSource().getFeatures().length > 0) {
                map.getView().fit(reportLayer.getSource().getExtent(), map.getSize());
            } else {
                const searchTerm = document.getElementsByClassName("searchTerm")[0].value;
                alert('No data found for ' + searchTerm);
            }
        }
        changeCount++;
    });

    map.on('pointermove', function(e){
      var pixel = map.getEventPixel(e.originalEvent);
      var hit = map.hasFeatureAtPixel(pixel);
      map.getViewport().style.cursor = hit ? 'pointer' : '';
    });

    map.on('click', function(event) {
        zikaData = [{}];
        let reportFound = false;
        map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {
            $('.zikaTable').show();
            $('#utilDiv').show();
//            if(feature.get('zikaCount') > 50000) {
//                window.location.href ='https://media0.giphy.com/media/IL1sMUfQVRNFC/source.gif';
//            }
            reportFound = true;
            let locationClicked = false;
            locationsViewed.forEach(location => {
                if(location.reportDate === feature.get('reportDate') &&
                   location.location === feature.get('location') &&
                   location.dataField === feature.get('dataField')) {
                    locationClicked = true;
                }
            });
            if(!locationClicked) {
                $('#zikaTableBody').append(`
                    <tr>
                        <td>${feature.get('reportDate')}</td>
                        <td>${feature.get('location')}</td>
                        <td>${feature.get('dataField')}</td>
                        <td>${feature.get('zikaCount')}</td>
                    </tr>`
                );
            }

            if(!locationClicked) {
                const reportData = {
                    reportDate: feature.get('reportDate'),
                    location: feature.get('location'),
                    dataField: feature.get('dataField')
                };
                locationsViewed.push(reportData);
            }
        });
    });

    $("#dateSelect").change(function(event){
        reportSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: '/api/reports/search/date?date=' + event.target.value
        });
        reportLayer.setSource(reportSource);
        map.removeLayer(reportLayer);
        map.addLayer(reportLayer);
    });

    $(".searchButton").click(function(event) {
        const searchTerm = document.getElementsByClassName("searchTerm")[0].value;
        const searchFor = document.getElementById("searchSelect").value;
        let searchUrl = '/api/reports/search?q=' + searchTerm;
        if(searchFor === 'dataField') {
            searchUrl = '/api/reports/search/datafield?data=' + searchTerm
        }        reportSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: searchUrl
        });
        reportLayer.setSource(reportSource);
        map.removeLayer(reportLayer);
        map.addLayer(reportLayer);
    });



//    const draw = new ol.interaction.Draw({
//      source: reportSource,
//      type: "Polygon"
////      geometryFunction: geometryFunction,
////      maxPoints: maxPoints
//    });
//    map.addInteraction(draw);
//
//    draw.on('drawend', function(event) {
//        console.log('done drawing');
//        console.log(event.feature.getGeometry().getCoordinates());
//        const features = reportLayer.getSource().getFeatures();
//        console.log(features);
//        map.removeInteraction(draw);
//    });
});

function clearTable() {
    $('#zikaTableBody tr').remove();
    $('.zikaTable').hide();
    $('#utilDiv').hide();
    locationsViewed = [];
}

function setupLayer(checkBox, geoLayer, geoSource) {
    let sourceUrl = '';
    switch(geoSource) {
        case 'navy':
            sourceUrl = 'https://geoint.nrlssc.navy.mil/nrltileserver/wms?';
            break;
        case 'SEDAC':
            sourceUrl = 'https://sedac.ciesin.columbia.edu/geoserver/wms';
            break;
        default:
            sourceUrl = 'http://13.59.142.140:8989/geoserver/wms';
            break;
    }

    if(checkBox.checked) {
        var params = {'LAYERS': geoLayer,
            'TILED': true
        };

        layer = new ol.layer.Tile({
            source: new ol.source.TileWMS(({
            url: sourceUrl,
            params: params
            }))
        });
        const addedLayer = {
            geoSource: geoLayer,
            geoLayer: layer
        }
        geoserverLayers.push(addedLayer);
        map.addLayer(layer);
        if(geoSource !== 'lc') {
            map.removeLayer(reportLayer);
            map.addLayer(reportLayer);
        }
    } else {
        geoserverLayers.forEach(layer => {
            if(layer.geoSource === geoLayer) {
                map.removeLayer(layer.geoLayer);
            }
        });
    }
}