package org.launchcode.zikaDashboard.data;

import org.launchcode.zikaDashboard.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
    Location findByCityAndName(String city, String name);

    List<Location> findByName(String name);
}
