package org.launchcode.zikaDashboard.data;

import org.elasticsearch.index.query.QueryBuilder;
import org.launchcode.zikaDashboard.es.ReportDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ReportDocumentRepository extends ElasticsearchRepository<ReportDocument, String> {
    Iterable<ReportDocument> search(QueryBuilder queryBuilder);
}
