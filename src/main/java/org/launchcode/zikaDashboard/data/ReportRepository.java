package org.launchcode.zikaDashboard.data;

import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
    List<Report> findByReportDateContaining(String date);

    @Query(value = "SELECT DISTINCT report_date from report ORDER BY report_date DESC", nativeQuery = true)
    List<String> findDistinctDates();

    public List<Report> findByLocationStartingWithIgnoreCase(String location);
}
