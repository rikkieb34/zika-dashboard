package org.launchcode.zikaDashboard.models;

import javax.persistence.*;

@Entity
public class Report {

    private String reportDate;
    private String location;
    private String locationType;
    private String dataField;
    private String dataFieldCode;
    private String timePeriod;
    private String timePeriodType;
    private Integer zikaCount;
    private String unit;

    @ManyToOne
    private Location state;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long reportId;

    // required Hibernate constructor
    public Report() {}

    public Report(String reportDate, String location, String locationType, String dataField, String dataFieldCode,
                  String timePeriod, String timePeriodType, Integer zikaCount, String unit) {
        this.reportDate = reportDate;
        this.location = location;
        this.locationType = locationType;
        this.dataField = dataField;
        this.dataFieldCode = dataFieldCode;
        this.timePeriod = timePeriod;
        this.timePeriodType = timePeriodType;
        this.zikaCount = zikaCount;
        this.unit = unit;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTimePeriodType() {
        return timePeriodType;
    }

    public void setTimePeriodType(String timePeriodType) {
        this.timePeriodType = timePeriodType;
    }

    public Integer getZikaCount() {
        return zikaCount;
    }

    public void setZikaCount(Integer zikaCount) {
        this.zikaCount = zikaCount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public Location getState() {
        return state;
    }

    public void setState(Location state) {
        this.state = state;
    }
}