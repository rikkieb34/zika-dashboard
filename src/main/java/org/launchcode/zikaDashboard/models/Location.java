package org.launchcode.zikaDashboard.models;

import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Location {

    private String name;
    private String city;
    private Geometry locationGeometry;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany
    @JoinColumn(name = "state_id")
    private List<Report> reports = new ArrayList<>();

    public Location() {}

    public Location(String name, String city, Geometry locationGeometry) {
        this.name = name;
        this.city = city;
        this.locationGeometry = locationGeometry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Geometry getLocationGeometry() {
        return locationGeometry;
    }

    public void setLocationGeometry(Geometry locationGeometry) {
        this.locationGeometry = locationGeometry;
    }

    public List<Report> getReports() {
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }
}
