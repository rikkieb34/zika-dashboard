package org.launchcode.zikaDashboard.controllers.es;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.*;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.data.ReportDocumentRepository;
import org.launchcode.zikaDashboard.es.ReportDocument;
import org.launchcode.zikaDashboard.features.Feature;
import org.launchcode.zikaDashboard.features.FeatureCollection;
import org.launchcode.zikaDashboard.models.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/api/reports")
public class ReportDocumentController {

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @GetMapping(value = "search")
    public FeatureCollection search(@RequestParam String q) {
        FeatureCollection featureCollection = new FeatureCollection();
        SearchQuery fuzzyQueryBuilder = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchQuery("location", q).fuzziness(2))
                .addAggregation(AggregationBuilders.terms("distinct_location").field("location.keyword").size(100))
                .build();
        Aggregations aggregations = elasticsearchTemplate.query(fuzzyQueryBuilder, new ResultsExtractor<Aggregations>() {
            @Override
            public Aggregations extract(SearchResponse response) {
                return response.getAggregations();
            }
        });

        // Loop through the distinct locations and add the properties to the feature collection
        StringTerms distinctLocations = aggregations.get("distinct_location");
        for (Terms.Bucket bucket : distinctLocations.getBuckets()) {
            String key = bucket.getKeyAsString();
            String[] locationArray = key.split("-");
            String distLocation = locationArray[0];
            if (locationArray.length > 1) {
                distLocation = locationArray[1];
            }

            // Get the sum of the zika counts for each location
            SearchQuery sumQueryBuilder = new NativeSearchQueryBuilder()
                    .withQuery(QueryBuilders.matchQuery("location", distLocation))
                    .withPageable(PageRequest.of(0, 250000))
                    .addAggregation(AggregationBuilders.sum("sum").field("zikaCount"))
                    .build();
            Aggregations sumAggregations = elasticsearchTemplate.query(sumQueryBuilder, new ResultsExtractor<Aggregations>() {
                @Override
                public Aggregations extract(SearchResponse response) {
                    return response.getAggregations();
                }
            });
            Sum sum = sumAggregations.get("sum");
            double zikaCount = sum.getValue();

            // Get the report dates range of zika reports for the each location
            SearchQuery dateQueryBuilder = new NativeSearchQueryBuilder()
                    .withQuery(QueryBuilders.matchQuery("location", distLocation))
                    .withSort(SortBuilders.fieldSort("reportDate").order(SortOrder.ASC))
                    .withPageable(PageRequest.of(0, 250000))
                    .build();

            List<ReportDocument> iterator = reportDocumentRepository.search(dateQueryBuilder).getContent();
            String reportDate = iterator.get(0).getReportDate() + " to " + iterator.get(iterator.size() - 1).getReportDate();
            String dataField = "total zika reported for " + q;
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("reportDate", reportDate);
            properties.put("location", key);
            properties.put("dataField", dataField);
            properties.put("zikaCount", zikaCount);
            String city = "";
            String name = key;
            if (key.indexOf("-") > 0) {
                city = key.substring(key.indexOf("-") + 1).replace("-", " ");
                name = key.substring(0, key.indexOf("-"));
            }
            Location location = locationRepository.findByCityAndName(city, name);
            if (location != null && location.getLocationGeometry() != null && zikaCount > 0) {
                featureCollection.addFeature(new Feature(location.getLocationGeometry(), properties));
            }
        }

        return featureCollection;
    }

    @GetMapping("search/date")
    public FeatureCollection getReportsByDate(@RequestParam String date) {
        FeatureCollection featureCollection = new FeatureCollection();
        SearchQuery fuzzyQueryBuilder = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchQuery("reportDate", date))
                .addAggregation(AggregationBuilders.terms("distinct_location").field("location.keyword").size(100))
                .build();


        Aggregations aggregations = elasticsearchTemplate.query(fuzzyQueryBuilder, new ResultsExtractor<Aggregations>() {
            @Override
            public Aggregations extract(SearchResponse response) {
                return response.getAggregations();
            }
        });

        // Loop through the distinct locations and add the properties to the feature collection
        StringTerms distinctLocations = aggregations.get("distinct_location");
        for (Terms.Bucket bucket : distinctLocations.getBuckets()) {
            String key = bucket.getKeyAsString();
            String distLocation = key;
            if(key.indexOf("-") > 0) {
                distLocation = key.substring(key.indexOf("-") + 1);
                distLocation = distLocation.replace("-", " ");
            }

            BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
            boolQueryBuilder.must(QueryBuilders.matchQuery("reportDate", date));
            boolQueryBuilder.must(QueryBuilders.matchQuery("location", distLocation).fuzziness(0));
            // Get the sum of the zika counts for each location
            SearchQuery sumQueryBuilder = new NativeSearchQueryBuilder()
                    .withQuery(boolQueryBuilder)
                    .withPageable(PageRequest.of(0, 250000))
                    .addAggregation(AggregationBuilders.sum("sum").field("zikaCount"))
                    .build();
            Aggregations sumAggregations = elasticsearchTemplate.query(sumQueryBuilder, new ResultsExtractor<Aggregations>() {
                @Override
                public Aggregations extract(SearchResponse response) {
                    return response.getAggregations();
                }
            });
            Sum sum = sumAggregations.get("sum");
            double zikaCount = sum.getValue();

            String dataField = "total zika reported";
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("reportDate", date);
            properties.put("location", key);
            properties.put("dataField", dataField);
            properties.put("zikaCount", zikaCount);
            String city = "";
            String name = key;
            if (key.indexOf("-") > 0) {
                city = key.substring(key.indexOf("-") + 1).replace("-", " ");
                name = key.substring(0, key.indexOf("-"));
            }
            Location location = locationRepository.findByCityAndName(city, name);
            if (location != null && location.getLocationGeometry() != null && zikaCount > 0) {
                featureCollection.addFeature(new Feature(location.getLocationGeometry(), properties));
            }
        }

        return featureCollection;
    }

    @GetMapping("search/datafield")
    public FeatureCollection getReportsByDataField(@RequestParam String data) {
        FeatureCollection featureCollection = new FeatureCollection();
        SearchQuery fuzzyQueryBuilder = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchQuery("dataField", data).fuzziness(2))
                .addAggregation(AggregationBuilders.terms("distinct_location").field("location.keyword").size(100))
                .build();
        Aggregations aggregations = elasticsearchTemplate.query(fuzzyQueryBuilder, new ResultsExtractor<Aggregations>() {
            @Override
            public Aggregations extract(SearchResponse response) {
                return response.getAggregations();
            }
        });

        // Loop through the distinct locations and add the properties to the feature collection
        StringTerms distinctLocations = aggregations.get("distinct_location");
        for (Terms.Bucket bucket : distinctLocations.getBuckets()) {
            String key = bucket.getKeyAsString();
            String[] locationArray = key.split("-");
            String distLocation = locationArray[0];
            if (locationArray.length > 1) {
                distLocation = locationArray[1];
            }

            BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
            boolQueryBuilder.must(QueryBuilders.matchQuery("dataField", data));
            boolQueryBuilder.must(QueryBuilders.matchQuery("location", distLocation).fuzziness(0));
            // Get the sum of the zika counts for each location
            SearchQuery sumQueryBuilder = new NativeSearchQueryBuilder()
                    .withQuery(boolQueryBuilder)
                    .withPageable(PageRequest.of(0, 250000))
                    .addAggregation(AggregationBuilders.sum("sum").field("zikaCount"))
                    .build();
            Aggregations sumAggregations = elasticsearchTemplate.query(sumQueryBuilder, new ResultsExtractor<Aggregations>() {
                @Override
                public Aggregations extract(SearchResponse response) {
                    return response.getAggregations();
                }
            });
            Sum sum = sumAggregations.get("sum");
            double zikaCount = sum.getValue();

            if(zikaCount < 1) {
                continue;
            }
            // Get the report dates range of zika reports for the each location
            SearchQuery dateQueryBuilder = new NativeSearchQueryBuilder()
                    .withQuery(boolQueryBuilder)
                    .withSort(SortBuilders.fieldSort("reportDate").order(SortOrder.ASC))
                    .withPageable(PageRequest.of(0, 250000))
                    .build();

            List<ReportDocument> iterator = reportDocumentRepository.search(dateQueryBuilder).getContent();
            String reportDate = iterator.get(0).getReportDate() + " to " + iterator.get(iterator.size() - 1).getReportDate();
            String dataField = "total zika reported for " + data;
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("reportDate", reportDate);
            properties.put("location", key);
            properties.put("dataField", dataField);
            properties.put("zikaCount", zikaCount);
            String city = "";
            String name = key;
            if (key.indexOf("-") > 0) {
                city = key.substring(key.indexOf("-") + 1).replace("-", " ");
                name = key.substring(0, key.indexOf("-"));
            }
            Location location = locationRepository.findByCityAndName(city, name);
            if (location != null && location.getLocationGeometry() != null && zikaCount > 0) {
                featureCollection.addFeature(new Feature(location.getLocationGeometry(), properties));
            }
        }

        return featureCollection;
    }
}

