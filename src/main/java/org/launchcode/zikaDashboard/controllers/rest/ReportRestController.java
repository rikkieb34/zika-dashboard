package org.launchcode.zikaDashboard.controllers.rest;

import org.launchcode.zikaDashboard.data.LocationRepository;
import org.launchcode.zikaDashboard.data.ReportDocumentRepository;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.es.ReportDocument;
import org.launchcode.zikaDashboard.features.Feature;
import org.launchcode.zikaDashboard.features.FeatureCollection;
import org.launchcode.zikaDashboard.models.Location;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/reports")
public class ReportRestController {
    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @RequestMapping(value = "")
    @ResponseBody
    public FeatureCollection getReports(@RequestParam(name = "date") Optional<String> date) {
        List<Report> reports = reportRepository.findByReportDateContaining(date.get());

        if(reports.isEmpty()) { return new FeatureCollection(); }

        FeatureCollection featureCollection = new FeatureCollection();
        for (Report report : reports) {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("reportDate", report.getReportDate());
            properties.put("location", report.getLocation());
            properties.put("locationType", report.getLocationType());
            properties.put("dataField", report.getDataField());
            properties.put("dataFieldCode", report.getDataFieldCode());
            properties.put("timePeriod", report.getTimePeriod());
            properties.put("timePeriodType", report.getTimePeriodType());
            properties.put("zikaCount", report.getZikaCount());
            properties.put("unit", report.getUnit());
            String[] city = report.getLocation().replace('_', ' ').split("-");
            Location location;
            if(city == null) {
                continue;
            } else if (city.length < 2) {
                location = locationRepository.findByCityAndName("", city[0]);
            } else {
                location = locationRepository.findByCityAndName(city[1], city[0]);
            }
            if(location != null && location.getLocationGeometry() != null)
                featureCollection.addFeature(new Feature(location.getLocationGeometry(), properties));
        }
        return featureCollection;
    }

    @PostMapping(value = "")
    @ResponseStatus(HttpStatus.CREATED)
    public Report postReport(@RequestBody Report report) {
        Report savedReport = reportRepository.save(report);
        reportDocumentRepository.save(new ReportDocument(savedReport));
        return savedReport;
    }


    @RequestMapping(value = "dates")
    public List<String> getDates() {
        List<String> dates = reportRepository.findDistinctDates();
        List<String> cleanDates = new ArrayList<String>();
        for (String date: dates) {
            cleanDates.add(date.split(" ")[0]);
        }
        return cleanDates;
    }

    @PostMapping(value = "/assignStates")
    public ResponseEntity<String> assignStates() {
        List<Location> locations = locationRepository.findAll();

        for(Location location: locations) {
            String country = location.getName();
            String state = location.getCity().replace(" ", "-");
            List<Report> matchingReports = reportRepository.findByLocationStartingWithIgnoreCase(country + "-" + state);
            for(Report report: matchingReports) {
                report.setState(location);
            }
            reportRepository.saveAll(matchingReports);
        }
        return new ResponseEntity<>("Complete", HttpStatus.OK);
    }
}
