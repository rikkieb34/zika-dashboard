package org.launchcode.zikaDashboard.util;

import org.launchcode.zikaDashboard.data.ReportDocumentRepository;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.es.ReportDocument;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EsUtil {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    public void refresh() {
        reportDocumentRepository.deleteAll();
        List<ReportDocument> reportDocuments = new ArrayList<>();
        int count = 0;
        for(Report report : reportRepository.findAll()) {
            reportDocuments.add(new ReportDocument(report));
            if(count % 1000 == 0) {
                reportDocumentRepository.saveAll(reportDocuments);
                reportDocuments.clear();
            }
            count++;
        }
        reportDocumentRepository.saveAll(reportDocuments);
    }
}
