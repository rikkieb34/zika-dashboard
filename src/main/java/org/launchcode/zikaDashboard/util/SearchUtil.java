package org.launchcode.zikaDashboard.util;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

public class SearchUtil {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    public SearchUtil() {}

    public SearchQuery findDistinctValues(String column, String q, String term, String fieldName) {
        SearchQuery fuzzyQueryBuilder = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.fuzzyQuery(column, q))
                .withPageable(PageRequest.of(0, 100))
                .addAggregation(AggregationBuilders.terms(term).field(fieldName))
                .build();

        return fuzzyQueryBuilder;
    }
}
