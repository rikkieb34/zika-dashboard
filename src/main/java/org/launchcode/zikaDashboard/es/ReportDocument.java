package org.launchcode.zikaDashboard.es;

import org.launchcode.zikaDashboard.models.Report;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Document(indexName = "#{esConfig.indexName}", type = "reports")
public class ReportDocument {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

    private String reportDate;
    private String location;
    private String locationType;
    private String dataField;
    private Integer zikaCount;

    public ReportDocument() {}

    public ReportDocument(Report report) {
        this.reportDate = report.getReportDate();
        this.location = report.getLocation();
        this.locationType = report.getLocationType();
        this.dataField = report.getDataField();
        this.zikaCount = report.getZikaCount();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public Integer getZikaCount() {
        return zikaCount;
    }

    public void setZikaCount(Integer zikaCount) {
        this.zikaCount = zikaCount;
    }
}
